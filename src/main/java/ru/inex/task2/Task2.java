package ru.inex.task2;

import ru.inex.runner.Task;

import java.util.List;

import static java.util.Arrays.asList;
import static ru.inex.task2.Station.station;

public class Task2 implements Task {

    private static final int INDEXED_FIRST_LETTERS_COUNT = 2;
    private static final boolean FULL_SCAN_ENABLED = false;

    @Override
    public void run() {
        List<Station> stations = asList(
                station("МОСКВА"),
                station("МОЖГА"),
                station("МОЗДОК"),
                station("САНКТ-ПЕТЕРБУРГ"),
                station("САМАРА")
        );

        StationFastStorage stationStorage = new StationFastStorage(
                stations, INDEXED_FIRST_LETTERS_COUNT, FULL_SCAN_ENABLED
        );

        System.out.println("МО: " + stationStorage.getStationsByIndexedFirstLetters("МО"));
        System.out.println("СА: " + stationStorage.getStationsByIndexedFirstLetters("СА"));
    }

    @Override
    public String description() {
        return "Вывод подходящих станций по первым " + INDEXED_FIRST_LETTERS_COUNT + " буквам";
    }
}
