package ru.inex;

import ru.inex.runner.TaskRunner;
import ru.inex.task1.Task1;
import ru.inex.task2.Task2;
import ru.inex.task3.Task3;

import static java.util.Arrays.asList;

public class Application {

    public static void main(String[] args) {
        TaskRunner taskRunner = new TaskRunner(asList(new Task1(), new Task2(), new Task3()));
        taskRunner.executeAll();
    }
}