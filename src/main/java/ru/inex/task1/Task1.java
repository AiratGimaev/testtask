package ru.inex.task1;


import ru.inex.runner.Task;

import java.util.Arrays;
import java.util.List;

import static ru.inex.task1.Version.version;

public class Task1 implements Task {

    @Override
    public void run() {
        List<Version> versions = Arrays.asList(
                version("1.1.1"),
                version("2.1.1"),
                version("1.2.3"),
                version("2.3.1"),
                version("1.1.3"),
                version("3.1.1"),
                version("1.1.2"),
                version("2.2.1"),
                version("1.2.4"),
                version("3.2.1")
        );
        VersionSorter.sort(versions);
        System.out.println(versions);
    }

    @Override
    public String description() {
        return "Сортировка версий";
    }
}
