package ru.inex.task1;

public class Version {

    private String version;
    private Integer majorPart;
    private Integer minorPart;
    private Integer bugfixPart;

    private Version(String version, Integer majorPart, Integer minorPart, Integer bugfixPart) {
        this.version = version;
        this.majorPart = majorPart;
        this.minorPart = minorPart;
        this.bugfixPart = bugfixPart;
    }

    public static Version version(String version) {
        String[] versionParts = version.split("\\.");
        if (versionParts.length != 3) {
            throw new IllegalArgumentException("Version = " + version + " does not match to template = #.#.#");
        }
        Integer major = Integer.parseInt(versionParts[0]);
        Integer minor = Integer.parseInt(versionParts[1]);
        Integer bugfix = Integer.parseInt(versionParts[2]);
        return new Version(version, major, minor, bugfix);
    }

    public Integer getMajorPart() {
        return majorPart;
    }

    public Integer getMinorPart() {
        return minorPart;
    }

    public Integer getBugfixPart() {
        return bugfixPart;
    }

    @Override
    public String toString() {
        return version;
    }
}
