package ru.inex.runner;

import java.util.List;

public class TaskRunner {

    private List<Task> tasks;

    public TaskRunner(List<Task> tasks) {
        this.tasks = tasks;
    }

    public void executeAll() {
        for (Task task : tasks) {
            System.out.println("Задача: " + task.description() + ". Стартовала...\n");
            task.run();
            System.out.println("\nЗадача: " + task.description() + ". Успешно завершилась.\n\n");
        }
    }
}
