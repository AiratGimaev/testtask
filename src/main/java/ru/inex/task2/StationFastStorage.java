package ru.inex.task2;

import java.util.*;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class StationFastStorage {

    private final int indexedFirstLettersCount;
    private final Map<String, List<Station>> stationMap;

    private boolean fullScanEnabled;

    public StationFastStorage(List<Station> stationList,
                              int indexedFirstLettersCount,
                              boolean fullScanEnabled) {
        this.indexedFirstLettersCount = indexedFirstLettersCount;
        this.fullScanEnabled = fullScanEnabled;
        Function<Station, String> indexer = station -> station.getName().substring(0, indexedFirstLettersCount);
        stationMap = stationList.stream().collect(groupingBy(indexer));
    }

    public Collection<Station> getStationsByIndexedFirstLetters(String prefix) {
        if (canUseIndexing(prefix)) {
            return stationMap.get(prefix);
        } else if (isFullScanEnabled()) {
            return stationMap
                    .values()
                    .stream()
                    .flatMap(Collection::stream)
                    .filter(station -> station.getName().startsWith(prefix))
                    .collect(toList());
        } else {
            throw new IllegalArgumentException("You have to check prefix length! " +
                    "It must be equal to " + indexedFirstLettersCount + ". You set it before in constructor. " +
                    "Also you can call enableFullScan() to activate full scan searching.");
        }
    }

    private boolean canUseIndexing(String prefix) {
        return prefix.length() == indexedFirstLettersCount;
    }

    public boolean isFullScanEnabled() {
        return fullScanEnabled;
    }

    public void enableFullScan() {
        this.fullScanEnabled = true;
    }

    public void disableFullScan() {
        this.fullScanEnabled = false;
    }

    public int getIndexedFirstLettersCount() {
        return indexedFirstLettersCount;
    }
}