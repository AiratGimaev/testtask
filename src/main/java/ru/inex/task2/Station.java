package ru.inex.task2;

public class Station {

    private String major;

    private Station(String major) {
        this.major = major;
    }

    public static Station station(String major) {
        return new Station(major);
    }

    public String getName() {
        return major;
    }

    @Override
    public String toString() {
        return major;
    }
}
