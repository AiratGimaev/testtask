package ru.inex.task1;

import java.util.List;

import static java.util.Comparator.comparing;

public class VersionSorter {

    public static void sort(List<Version> versions) {
        versions.sort(comparing(Version::getMajorPart)
                .thenComparing(Version::getMinorPart)
                .thenComparing(Version::getBugfixPart)
        );
    }
}

