package ru.inex.runner;

public interface Task {

    void run();

    String description();
}
