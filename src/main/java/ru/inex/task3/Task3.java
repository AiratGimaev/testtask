package ru.inex.task3;

import ru.inex.runner.Task;

import java.util.*;

import static java.util.Objects.isNull;

public class Task3 implements Task {

    @Override
    public void run() {
        int[] numbers = new int[]{0, 1, 2, 3, 5, 7, 11, 12, 16, 17};

        Map<Integer, Integer> sumToCountMap = groupBySumOfTwoNumbers(numbers);

        printUniqueSum(sumToCountMap);
    }

    private Map<Integer, Integer> groupBySumOfTwoNumbers(int[] numbers) {
        Map<Integer, Integer> sumToCountMap = new HashMap<>();

        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                count(sumToCountMap, numbers[i] + numbers[j]);
            }
        }
        return sumToCountMap;
    }

    private void count(Map<Integer, Integer> sumToCountMap, int sum) {
        Integer count = sumToCountMap.get(sum);
        if (isNull(count)) {
            sumToCountMap.put(sum, 1);
        } else {
            sumToCountMap.put(sum, count + 1);
        }
    }

    private void printUniqueSum(Map<Integer, Integer> sumToCountMap) {
        sumToCountMap
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() == 1)
                .forEach(entry -> System.out.println(entry.getKey()));
    }

    @Override
    public String description() {
        return "Вывод уникальных сумм пар чисел из заданного списка";
    }
}
